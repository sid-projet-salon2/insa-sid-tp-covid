#!/usr/bin/env python3
#Classe en majorité commentée, les méthode generate et str sont nos méthodes crées lors des Tps
import json
#for generation of messages
import random
from hashlib import *
import time

class Message:
	#some constants (message types)
	MSG_ISAID = 0 	#What-I-said message
	MSG_IHEARD = 1	#What-I-heard message
	MSG_THEY = 2	#What-covid-19-infected-said message
	#the constructor can take various parameters
	#default is parameterless, it generates a message to be said
	#if type is not specified, it is to be imported from an export string or parsed export string
	#otherwise, if date is False the time is system time
		#else it is a float, then the date should be this parameter
	def __init__(self, msg="", msg_type=False, msg_date=False): #on initialise un message sans contenu, sans type et sans date
		if msg == "": # si le contenu est vide
			self.content = self.generate() #on génère un message
			self.type = Message.MSG_ISAID #on lui attribue le type "j'ai dit"
			self.date = time.time() #on lui donne la date actuelle
		elif msg_type is False : #si le message doit être crée à partir d'un fichier json
			self.m_import(msg) #j'utilise la méthode import
		else: # si le contenu et le type sont indiqué
			self.content = msg #je lui attribue le contenu donné
			self.type = msg_type #je lui attribue le type donné
			if msg_date is False: #  si la date n'est pas indiqué
				self.date = time.time() #je lui attribue la date actuelle
			else:
				self.date = msg_date #sinon je lui attribue la date donnée

	#a method to print the date in a human readable form
	def hdate(self):
		return time.ctime(self.date) #affiche la date sous la forme Jour mois heure

	#a method to compute the age of a message in days or in seconds
	#for display purpose, set as_string to True
	def age(self, days=True, as_string=False): #méthode qui calcule la différences de temps entre la date du message et la date actuelle
		age = time.time() - self.date #l'age est la date actuelle moins la date du message
		if days:#si le temps se compte en jour
			age = int(age/(3600*24)) #je divise l'age en seconde par le nombre de seconde en 1 jour pour avoir la date en jour
		if as_string: #si je souhaite afficher le temps autrement que en seconde
			d = int(age/(3600*24)) #je calcule le nombre de jours entier passés
			r = age%(3600*24) #je retire le nombre de jours passés à l'age
			h = int(r/3600) #je calcule le nombre d'heures entiéres passées
			r = r%3600 #je retire le nombre d'heures passées à l'age
			m = int(r/60) #je calcule le nombre de minutes entières passées
			s = r%60 #je retire le nombre de minutes passées à l'age
			age = (str(age)+"~"+str(d)+"d"+ str(h) + "h"+str(m)+"m"+str(s)+"s") #j'affiche l'age suivant la nouvelle décomposition
		return age

	#testers of message type
	def is_i_said(self): #méthode pour définir le type j'ai dit à un message
		return self.type == Message.MSG_ISAID
	def is_i_heard(self): #méthode pour définir le type j'ai entendu à un message
		return self.type == Message.MSG_IHEARD
	def is_they_said(self): #méthode pour définir le type ils ont dit à un message
		return self.type == Message.MSG_THEY

	#setters
	def set_type(self, new_type): #méthode pour mettre à jour le type
		self.type = new_type

	#a class method that generates a random message
	@classmethod #Il s'agit de notre méthode de création de message
	def generate(self): #méthode pour génerer un message à partir d'une suite aléatoire de 1 million de chiffre et de l'heure actuelle que l'on brouille avec hash
		return hash(str(int(random.random()*10**6))+"-"+str(int(time.time())))


	#a method to convert the object to string data
	def __str__(self): #Notre méthode d'affichage du message : le contenu plus la date brouillée
		return (str(self.content)+str(hash(self.date)))

	#export/import
	def m_export(self): #méthode qui permet d'exporter le message sous format json
		return f"{{\"content\":\"{self.content}\",\"type\":{self.type}, \"date\":{self.date}}}"

	def m_import(self, msg): #méthode qui permet d'importer un message de format json
		if(type(msg) == type(str())):
			json_object = json.loads(msg)
		elif(type(msg) == type(dict())):
			json_object = msg
		else:
			raise ValueError
		self.content = json_object["content"] #attribution du contenu du message recupéré dans la borne "content" json à notre message
		self.type = json_object["type"] #attribution du type du message recupéré dans la ligne ayant pour id "Type"
		self.date = json_object["date"] #attribution de la date du message recupéré dans la ligne ayant pour id "date"

#will only execute if this file is run
if __name__ == "__main__":
	#test the class
	myMessage = Message() #création d'un message sans contenu, ni type, ni date
	time.sleep(1) #attente
	mySecondMessage = Message(Message.generate(), Message.MSG_THEY) #création d'un deuxième message sans conteny, de type ils ont dit et sans date
	copyOfM = Message(myMessage.m_export()) #export du premier message au format json
	print(myMessage) #affichages des messages
	print(mySecondMessage)
	print(copyOfM)
	time.sleep(0.5) 
	print(copyOfM.age(True,True)) #affichage de l'age de mon premier message
	time.sleep(0.5) #attente
	print(copyOfM.age(False,True)) #affichage de l'age de mon deuxième message
