#!/usr/bin/env python3
#Classe commentée représentant le catalogue des messages sous format json
import json, os
from message import *

class MessageCatalog:
	#constructor, loads the messages (if there are any)
	#in this implementation we store all messages in a json file
	def __init__(self, filepath): #initialisation du catalogue
		#            I_SAID,I_HEARD,THEY
		self.data = [  []  ,  []   , [] ] #création d'une matrice de 3 lignes vides visant à contenir les messages selon les 3 types
		self.filepath = filepath #recupération du chemin ou se situe le fichier json du catalogue
		if os.path.exists(self.filepath): #si le chemin existe donc s'il y a déjà des messages
			self.open() # j'ouvre le fichier
		else:
			self.save() # je crée le fichier

	#destructor closes file.
	def __del__(self): # méthode de suppresion du catalague
		del self.filepath # je supprime le chemin
		del self.data # je supprime les données
 
	#get catalog size
	def get_size(self, msg_type=False): # méthode pour récuperer la taille du catalogue
		if(msg_type is False): # si je veux la taille de TOUT le catalogue
			res = self.get_size(Message.MSG_ISAID) + self.get_size(Message.MSG_IHEARD) + self.get_size(Message.MSG_THEY) # je recupère la taille des 3 type de messages
		else:
			res = len(self.data[msg_type]) # je recupère uniquement la taille de la ligne contenant le type de message souhaité
		return res

	#Import object content
	def c_import(self, content, save=True): # méthode d'import de messages
		i = 0
		for msg in content: #pour chaque message
			#we don't save after adding message because we just
			#read the content
				if self.add_message(Message(msg), False): #j'ajoute le message au catalogue
					i=i+1
		if save: #je sauvegarde le catalogie
			self.save()
		return i

	#Import a set of messages but changing their type to new_type
	def c_import_as_type(self, content, new_type=Message.MSG_THEY): #méthode d'import de message en modifiant leur type: par defaut "ils ont dit"
		i=0
		for msg in content: #pour chaque message
			m = Message(msg) #je crée un nouveau message de  même contenu
			m.set_type(new_type) #je change son type
			if self.add_message(m, False): #je l'ajoute au catalogue
				i=i+1
		if i > 0: #si j'ai modifié au moins un message
			self.save() # je sauvegarde
		return i

	#Open = load from file
	def open(self): #méthode de lecture d'un fichier
		file = open(self.filepath,"r") # j'ouvre en mode lecture le fichier à partir du chemin 
		self.c_import(json.load(file), False) #j'importe les données
		file.close() #je referme le fichier

	#export the messages of a given type from a catalog
	def c_export_type(self, msg_type, indent=2, bracket=True): #méthode qui permet d'exporter les messages d'un certain type au format json
		tmp = int(bracket) * "[\n" #création du langage json
		first_item = True
		for msg in self.data[msg_type]: # pour chaque message du type indiqué
			if first_item: #si c'est le premier element je le définis comme tel
				first_item=False
			else:
				tmp = tmp +",\n"
			tmp = tmp + (indent*" ") +  msg.m_export() #j'exporte le message au format json
		tmp = tmp + int(bracket) * "\n]" # je ferme mon fichier json
		return tmp

	#Export the whole catalog under a text format
	def c_export(self, indent=2): #méthode qui permet d'exporter tout le catalogue au format json
		tmp = ""
		if(len(self.data[Message.MSG_ISAID])> 0 ): # si j'ai des message de type j'ai dit
			tmp = tmp + self.c_export_type(Message.MSG_ISAID, indent, False) # je les exporte 
		if(len(self.data[Message.MSG_IHEARD]) > 0): # si j'ai des messages de type j'ai entendu 
			if tmp != "":
				tmp = tmp + ",\n"
			tmp = tmp + self.c_export_type(Message.MSG_IHEARD, indent, False) # je les exporte 
		if(len(self.data[Message.MSG_THEY]) > 0): # si j'ai des messages de type ils ont dit
			if tmp != "":
				tmp = tmp + ",\n"
			tmp = tmp + self.c_export_type(Message.MSG_THEY, indent, False) # je les exporte 
		tmp = "[" + tmp + "\n]"
		return tmp

	#a method to convert the object to string data
	def __str__(self): #méthode qui pemret l'affichage des messages en HTML
		tmp="<catalog>\n"
		tmp=tmp+"\t<isaid>\n"
		for msg in self.data[Message.MSG_ISAID]: #pour les message de type j'ai dit 
			tmp = tmp+str(msg)+"\n" # je converti en string le contenu
		tmp=tmp+"\n\t</isaid>\n\t<iheard>\n"
		for msg in self.data[Message.MSG_IHEARD]: #pour les message de type j'ai entendu
			tmp = tmp+str(msg)+"\n" # je converti en string le contenu
		tmp=tmp+"\n\t</iheard>\n\t<theysaid>\n"
		for msg in self.data[Message.MSG_THEY]: #pour les message de type ils ont dit
			tmp = tmp+str(msg)+"\n" # je converti en string le contenu
		tmp=tmp+"\n\t</theysaid>\n</catalog>"
		return tmp

	#Save object content to file
	def save(self): # méthode de sauvegarde du fichier
		file = open(self.filepath,"w") # ouvre le fichier en ecriture
		file.write(self.c_export()) #exporte le catalogue
		file.close() # ferme le fichier
		return True

	#add a Message object to the catalog
	def add_message(self, m, save=True): #méthode d'ajout d'un message au catalogue
		res = True
		if(self.check_msg(m.content, m.type)): #je teste si le message existe déjà
			print(f"{m.content} is already there")
			res = False
		else: #si le message n'existe pas 
			self.data[m.type].append(m) #je rajoute le message a la fin de la ligne correspondant à son type
			if save:
				res = self.save() # j'enregistre le catalogue
		return res

	#remove all messages of msg_type that are older than max_age days
	def purge(self, max_age=14, msg_type=False): # méthode de suppression des messages trop vieux ( plus de 14 jours)
		if(msg_type is False): # si je n'indique pas de type 
			self.purge(max_age, Message.MSG_ISAID) # je supprime les messages du type j'ai dit
			self.purge(max_age, Message.MSG_IHEARD) # je supprime les messages du type j'ai entendu
			self.purge(max_age, Message.MSG_THEY) # je supprime les messages du type ils ont dit
		else:
			removable = [] # je crée une table vide des élement à supprimer
			for i in range(len(self.data[msg_type])): # pour chaque message de la ligne du type
				if(self.data[msg_type][i].age(True)>max_age): # si leur date est supérieure à la limite choisie ( ici 14j)
					removable.append(i) # je les ajoute à la table a supprimer
			while len(removable) > 0:# si la table est non vide
					del self.data[msg_type][removable.pop()] # je la supprime
			self.save() # je sauvegarde mon catalogue
		return True

	#Check if message string is in a category
	def check_msg(self, msg_str, msg_type=Message.MSG_THEY): # méthode qui teste si le message existe déjà dans la ligne des message de type ils ont dit 
		for msg in self.data[msg_type]: #pour chaque message du type j'ai entendu
			if msg_str == msg.content:# je teste si son contenu est le même que mon message
				return True
		return False

	#Say if I should quarantine based on the state of my catalog
	def quarantine(self, max_heard=4): #methode indiquant s'il faut s'isoler
		self.purge() # je supprime les messages de plus de 14 jours ( temps d'incubatio max)
		n = 0 # je definis mon nombre de contact à 0
		for msg in self.data[Message.MSG_IHEARD]: # pour chaque message du type j'ai entendu
			if self.check_msg(msg.content): # je teste s'il fait parti des messages ils ont dit
				n = n + 1 # si oui j'ajoute un contact
		print(f"{n} covid messages heard")
		return max_heard < n # si le nombre de message entendu de personne testées positive est supérieur à 4 ( temps de contact suffisamment long) je dois m'isoler

#will only execute if this file is run
if __name__ == "__main__": #méthode main de test
	#test the class
	catalog = MessageCatalog("test.json") # je crée un catalogue test
	catalog.add_message(Message()) # je lui ajoute un message qui sera par défaut de type j'ai dit 
	catalog.add_message(Message(Message.generate(), Message.MSG_IHEARD)) # je lui ajoute un message de type j'ai entendu
	print(catalog.c_export()) #j'exporte mon catalogue au format json et je l'affiche 
	time.sleep(0.5) # j'attend
	catalog.add_message(Message(f"{{\"content\":\"{Message.generate()}\",\"type\":{Message.MSG_THEY}, \"date\":{time.time()}}}")) # j'ajoute un message du type ils ont dit 
	catalog.add_message(Message()) # je lui ajoute un message qui sera par défaut de type j'ai dit 
	print(catalog.c_export()) #j'exporte mon catalogue au format json et je l'affiche 
	time.sleep(0.5) #j'attend
	catalog.add_message(Message())  # je lui ajoute un message qui sera par défaut de type j'ai dit 
	print(catalog.c_export()) #j'exporte mon catalogue au format json et je l'affiche 
	time.sleep(2) #j'attend
	catalog.purge(2) # je purge les message trop vieux d'age supérieur à 2 sec
	print(catalog) # j'affiche mon catalogue
