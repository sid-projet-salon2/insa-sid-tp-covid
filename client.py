#!/usr/bin/env python3
#Commentaire du code et ajout en commentaire de notre methode say something
from message import Message
from message_catalog import MessageCatalog
import requests

class Client: # creation de la classe client qui est la personne communiquant avec le serveur : il s'agit de la personne physique qui envoie et ecoute les messages et qui peut tomber malade 
	#constructor, takes a message catalog
	def __init__(self, catalog_path, debug = False, defaultProtocol="http://"): # creation du client avec son catalogue de message
		self.catalog = MessageCatalog(catalog_path) # je cree un catalogue propre au client
		if debug:
			print(self.catalog) # j'affiche le catalogue
		self.r = None # je cree l'attrbut qui contiendra les requetes
		self.debug = debug #attribut de test
		self.protocol = defaultProtocol #definiton du protocole par d�faut : http://
        
	@classmethod
	def withProtocol(cls, host): #methode permettant de tester si le protocole est d�j� d�fini ou non
		res = host.find("://") >= 1 #retourne TRUE s'il y a "://" dans l'hote, donc si le protocole est d�j� d�fini
		return res

	def completeUrl(self, host, route = ""):#methode permettant de creer la route pour la requete en sorte que l'url soit toujours de la forme protocole + nom de domaine + contenu s'il y en a un
		if not Client.withProtocol(host): #si le protocole n'est pas donn�
			host = self.protocol + host #je defini l'hote comme le protocole par d�faut + l'hote donnee en entree
		if route != "": #si la route n'est pas vide donc si j'ai un contenu � faire passer
			route = "/"+str(route) #je defini la route comme / suivi du texte donn� en entr�e
		return host+route #je retourne l'hote (qui correspond � protocole + nom de domaine) puis la route ce qui cree l'URL de communication

	#send an I said message to a host
	def say_something(self, host): # methode faisant le client envoie un message au serveur
		m = Message() #creation du message
		self.catalog.add_message(m) # ajout du message a son catalogue j'ai dit
		route = self.completeUrl(host, m.content) # je cree la route pour transmettre le message en donnant en parametre l'hote et le contenu du message 
		self.r = requests.post(route) # je fais une requete de transmissions
		if self.debug: # si je souhaite tester mon code
			print("POST  "+route + "→" + str(self.r.status_code)) # j'affiche le fait que je fais un post donc que je poste une info, la route et le statut de la requete
			print(self.r.text) # j'affiche la requete
		return self.r.status_code == 201 #je returne TRUE si le statut de la requete est 201"Created" qui veut dire requete traitée avec succès et création d'un document
   
    #def say_something(self,host):
     #   m="Salut ça va ?"
      #  r=requests.post(host+"/"+m)
       # if r.status_code==201:
        #    self.catalogue.append(m)
         #   print(m+" : ajoute")
        #else:
         #   print("il y a eu une erreur")

	#add to catalog all the covid from host server
	def get_covid(self, host): #Méthode qui recupère les messages des gens qui ont eu le covid message de type ils ont dit
		route = self.completeUrl(host,'/they-said') # je créer la route
		self.r = requests.get(route) # je crée la requete
		res = self.r.status_code == 200 # je donne a res la valeur TRUE si le statut de ma requete est égal �  200 qui veut dit ok ressource trouvée
		if res:# si la ressource est trouvée
			res = self.catalog.c_import(self.r.json()) # j'importe les messages
		if self.debug: # si je souhaite debuger j'affiche la route
			print("GET  "+ route + "→" + str(self.r.status_code)) # j'affiche que je fais un "get" donc que je veux recupérer des données, la route et le statut de la requette
			if res != False: #et si le statut de la requete est true
				print(str(self.r.json())) #j'affiche la requete
		return res

	#send to server list of I said messages
	def send_history(self, host): # méthode d'envoie au serveur des messages que j'ai dit
		route = self.completeUrl(host,'/they-said') # je crée la route
		self.catalog.purge(Message.MSG_ISAID) # je supprime les messages trop vieux
		data = self.catalog.c_export_type(Message.MSG_ISAID) # j'exporte les messages que j'ai dit 
		self.r = requests.post(route, json=data) # je fait une requete
		if self.debug: # pour debuger j'affiche la route, les message et la requete 
			print("POST  "+ route + "→" + str(self.r.status_code)) #j'affiche que je fais un "post" donc que je poste une info, la route et le statut de la requete
			print(str(data)) #j'affiche les données
			print(str(self.r.text)) #j'affiche la requette
		return self.r.status_code == 201#je retourne TRUE si le statut de la requete est égal �  201 "Created" qui veut dire requete traitée avec succès et création d'un document


if __name__ == "__main__": # méthode main de test
	c = Client("client.json", True) # je crée un client en mode debug
	c.say_something("localhost:5000") # le client dit des messages
	c.get_covid("localhost:5000") # le client recupère les messages des gens ayant eu le covid
	c.send_history("localhost:5000") # Le client envoie les messages qu'il a dit au serveur
