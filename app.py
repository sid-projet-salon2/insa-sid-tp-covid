#!/usr/bin/env python3
# commentaire du code et rajout en commentaire de notre methode add_heard
from flask import Flask #server
from flask import request #to handle the different http requests
from flask import Response #to reply (we could use jsonify as well but we handled it)
from flask import render_template #to use external files for responses (out of the scope of the project, added to provide a test UI)
import json
#My libraries
from message import Message
from client import Client #embarks message_catalog

app = Flask(__name__)

#“Routes” will handle all requests to a specific resource indicated in the
#@app.route() decorator

#These route are for the "person" use case
	#case 1 refuse GET
@app.route('/', methods=['GET']) #je me place en GET : je cherche a r�cuperer des donn�es
def index():
	response = render_template("menu.html",root=request.url_root, h="")
	return response

	#case 2 hear message
@app.route('/<msg>', methods=['POST']) #je me place en post: je cherche � poster de message qui suis le / 
def add_heard(msg): #methode utilisee lorsque le serveur recoit (entend) un message de la part d'un Client
	if client.catalog.add_message(Message(msg, Message.MSG_IHEARD)):#si il s'agit bien d'un message (bon format) et qu'il est ajout� au catalogue en lui attribuant le type message entendu
		response = Response(f"Message “{msg}” received.", mimetype='text/plain', status=201) #je renvoies au Client d'ou provient le message entendu (methode Response()) un message texte et le statut 201 c'est �  dire confirmation que le message a bien �t� recu
	else :
		reponse = Response(status=400) #si le message ne peut pas etre ajout� au catalogue alors on renvoie au Client d'où provient le message le status 400 qui signifie qu'il y a eu une erreur dans la réception du message
	return response
#nous avions ecrit la methode suivante avant la correction (elle nous permettait juste d'envoyer une réponse au Client quand le serveur entendait un message) :
    #def add_heard(msg):
        #print(msg)
        #return Response("bien recu "+msg, status=201)
        
#End of person use case

#Hospital use case
@app.route('/they-said', methods=['GET','POST']) #pour l'hopital je vais avoir besoinde GET: recuperer des donn�es ( liste des message they said) et du POST pour poster des messages si je suis atteint de la Covid19
def hospital(): #cette methode est utilis�e lorsqu'un Client demande la liste de tous les messages envoyés par des Clients réputés atteints de la Covid19 ou lorsqu'un client informe l'hôpital qu'il est atteint de la Covid19
	if request.method == 'GET': #j'utilise ce qui suit si le message du Client indique qu'il veut la liste des messages envoy�s par les Clients atteints de la Covid19, je me place donc en GET: r�ception de donn�es
		#Wants the list of covid messages (they said)
		client.catalog.purge(Message.MSG_THEY)#je supprime les messages de plus de 14 jours avec la fonction purge
		response = Response(client.catalog.c_export_type(Message.MSG_THEY), mimetype='application/json', status=200)#je renvoies la liste des messages en format JSON accompagn� du code status 200 (réussite de l'opération)
	elif request.method == 'POST':#j'utilise ce qui suit si le message du Client indique qu'il veut se déclarer comme contaminé par la Covid19
		if request.is_json:#je teste si le message envoyé est au format JSON
			req = json.loads(request.get_json())#je rentre le contenu du JSON dans une variable
			response = client.catalog.c_import_as_type(req, Message.MSG_THEY) #je rajoute les messages envoyés par le client dans le catalogue des messages avec le type "ils ont dit" c'est �  dire les "messages des covidés"
			response = Response(f"{response} new “they said” messages.", mimetype='text/plain', status=201)#je renvoies au client d'où provient le message une réponse disant que ses messages ont bien été enregistré avec le code de validation 201
		else:
			response = Response(f"JSON expected", mimetype='text/plain', status=400)#je renvoies au client d'où provient le message qu'il manque un JSON dans le fichier
	else:
		reponse = Response(f"forbidden method “{request.method}”.",status=403)#je renvoies au client d'où provient le message qu'il manque l'indication de ce qu'il souhaite (GET ou POST) accompagné du code d'erreur 403
	return response
#End hospital use case

#UI
#These routes are out of the scope of the project, they are here
#to provide a test interface
@app.route('/check/<host>', methods=['GET'])
def check(host):
	h = host.strip()
	n = client.get_covid(h)
	r = dict()
	r["text"] = f"{n} they said messages imported from {h} ({client.r.status_code})"
	if client.catalog.quarantine(4):
		r["summary"] = "Stay home, you are sick."
	else:
		r["summary"] = "Everything is fine, but stay home anyway."
	return render_template("menu.html",responses=[r],root=request.url_root, h=h)

@app.route('/declare/<host>', methods=['GET'])
def declare(host):
	h = host.strip()
	client.send_history(h)
	r=[{"summary":f"Declare covid to {h} ({client.r.status_code})",
	"text":client.r.text}]
	return render_template("menu.html",responses=r,root=request.url_root, h=h)

@app.route('/say/<hosts>', methods=['GET'])
def tell(hosts):
	hosts = hosts.split(",")
	r=[]
	for host in hosts:
		h = host.strip()
		client.say_something(h)
		r.append({"summary":f"ISAID to {h} ({client.r.status_code})",
		"text":client.r.text})
	return render_template("menu.html", responses=r, root=request.url_root, h=h)
#end UI

#will only execute if this file is run
if __name__ == "__main__":
	debugging = True
	client = Client("client.json", debugging)
	app.run(host="0.0.0.0", debug=debugging)